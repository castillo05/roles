'use strict'

let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let UserSchema=new Schema({
    name:String,
    email: String,
    password:String,
    id_role:{type:Schema.ObjectId,ref:'Roles'}
});

module.exports=mongoose.model('User',UserSchema);