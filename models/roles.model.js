'use strict'

let mongoose = require('mongoose');
let Schema =mongoose.Schema;

let RolesSchema=new Schema({
    role:String,
    description:String
});

module.exports=mongoose.model('Roles',RolesSchema);