'use strict'
const User = require('../models/user.model');
const bcrypt = require('bcrypt');
const jwt = require('../services/jwt');

function saveUser(req,res) {
    let U = new User();

    const saltRound=1;

    U.name=req.body.name;
    U.email=req.body.email;
    U.id_role=req.body.id_role;

    if(req.body.name === '' || req.body.email === '' || req.body.password === '' || req.body.role === ''){
        return res.status(200).send({message:'Favor completar todos los campos'});
    }

    let search = User.find({email:req.body.email.toLowerCase()}).sort();

    search.exec((err,user)=>{
        if(err) return res.status(500).send({message:err});

        if(user.length>=1){
            res.status(200).send({message:'Este email ya existe'});
        }else{
            if(req.body.password){
                bcrypt.hash(req.body.password, saltRound,(err,hash)=>{
                    if(err) return console.log(err);

                    U.password = hash;

                    U.save((err,userStorage)=>{
                        if(err) return console.log(err);

                        if (!userStorage) {
                            res.status(200).send({message:'El usuario no se ha registrado'});
                        }else{
                            res.status(200).send({user:userStorage});
                        }
                    })
                })
            }else{
                return res.status(500).send({message:'Introduzca la contraseña'});
            }
        }
    })
}

// Login
function login(req, res) {
    let email = req.body.email;

    let search = User.findOne({email:email.toLowerCase()});

    search.populate({path:'id_role'}).exec((err, user)=>{
        if(err) return console.log(err);

        if (!user) {
            res.status(200).send({message:'Este correo electronico no esta registrado'});
        } else {
            if(!req.body.password) return res.status(200).send({message:'Introduzca la contraseña'});

            bcrypt.compare(req.body.password, user.password,(err,check)=>{
                if(err) return res.status(200).send({message:'Contraseña invalida'});

                if(req.body.gethash){
                    res.status(200).send({token:jwt.createToken(user)});
                }else{
                    res.status(200).send({user:user});
                }
            })
        }
    })
}

module.exports={
    saveUser,
    login
}