'use strict'
const Role = require('../models/roles.model');

function saveRol(req, res) {
    let rol = new Role();

    rol.role=req.body.role;
    rol.description=req.body.description;

    rol.save((err,role)=>{
        if(err) return console.log(err);

        if (!role) {
            res.status(500).send({message:'Error del servidor'});
        } else {
            res.status(200).send({role:role});
        }
    });
}

module.exports={saveRol}