'use strict'

const express = require('express');

const bodyparser = require('body-parser');

const app = express();

// rutas
const userroute = require('./route/user.route');



app.use(bodyparser.urlencoded({extended:false,limit:'50mb',parameterLimit:50000}));
app.use(bodyparser.json({limit:'50mb'}));

//configurar cabeceras http
app.use((req,res,next)=>{
	res.header('Access-Control-Allow-Origin','*');
	res.header('Access-Control-Allow-Headers','Authorization, X-API-KEY,Origin,X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request');
	res.header('Access-Control-Allow-Methods','GET, POST, OPTIONS, PUT, DELETE');
	res.header('Allow','GET, POST, OPTIONS, PUT, DELETE');
	next();
});

app.get('/api', function (req, res) {
    res.status(200).send({message:'Bienvenido a la API'});


});

app.use('/api', userroute);


module.exports=app;