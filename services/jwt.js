'use strict'

const jwt = require('jwt-simple');
const moment = require('moment');

const secret = 'jcdevelopernicaragua';

exports.createToken=function (user) {
    let payload={
        sub:user._id,
        name:user.name,
        email:user.email,
        id_role:user.id_role,
        iat:moment().unix(),
        exp:moment().add(30,'days').unix
    }

    return jwt.encode(payload,secret);
}