'use strict'

const express = require('express');

const api = express.Router();

// Middleware de autorizacion
const auth = require('../middelware/authenticate');


const userController = require('../controllers/user');

const roleControler = require('../controllers/role');

const adminController = require('../controllers/admin');

const guestController = require('../controllers/guest');

api.post('/singup', userController.saveUser);
api.post('/login', userController.login);

// Ruta de role
api.post('/createrole', auth.authorization,auth.validate, roleControler.saveRol);

// Rutas de admin y guest
api.get('/admin',auth.authorization,auth.validate, adminController.admin);

api.get('/guest', auth.authorization,guestController.guest);



module.exports=api;