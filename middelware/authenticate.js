'use strict'

const jwt = require('jwt-simple');

const moment = require('moment')
const secret ='jcdevelopernicaragua';

exports.authorization=function(req, res, next){
    if(!req.headers.authorization){
        return res.status(403).send({message:'La peticion no tiene la cabecera de autenticacion'});
    }

    var token = req.headers.authorization.replace(/['"]+/g,'');

    try {
        var payload = jwt.decode(token,secret);
        if (payload.exp<=moment().unix()) {
            return res.status(401).send({message:'El token ha expirado'});
        }
    } catch (error) {
        return res.status(404).send({message:'Token no valido'});
    }

    req.user=payload;
   
    next(); 

    console.log(payload);
}

exports.validate=(req, res, next)=>{
    let user = req.user;
    if(user.id_role.role === 'administrator'){
        next();
    }else{
        res.status(200).send({message:'Permiso denegado'});
    }    
}